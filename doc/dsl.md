# Kapigen
#### Kateops Pipeline Generator (DSL)

Kapigen CLI is a tool to generate GitLab pipelines based on configuration files.
It abstracts the `.gitlab-ci.yml` syntax GitLab uses, enabling developers to leverage GitLab's CI/CD capabilities without using YAML and instead using Golang.

----

## [Pipeline Generation Documentation](index.md)

### Quick start

Kapigen allows you to generate pipelines based on the configuration defined in your repository.
To use it inside your project, include the following in your `.gitlab-ci.yml` file:

```yaml
include:
  - project: 'kateops/kapigen'
    file: 'default.gitlab-ci.yml'
```

Default Tag: `${KAPIGEN_DEFAULT_RUNNER_TAG}`

Can either be set in the `.gitlab-ci.yml` file as variable, an instance level variable or a project level variable. 
You may overwrite the tags for the pipeline generation by:

```yaml
default:
  tags:
    - saas-linux-medium-amd64
```

### Using the DSL

#### Creating a Custom Pipeline with DSL

You can create custom pipelines using the DSL provided by Kapigen. Here is an example of a project containing a `.kapigen` folder with a `main.go` inside:

Create a go.mod file inside the `.kapigen` folder and get the required packages:

```bash
go mod init kapigen
go get gitlab.com/kateops/kapigen/dsl@latest
````


##### .kapigen/main.go

```go
package main

import (
	"gitlab.com/kateops/kapigen/dsl/pipelines"
	"gitlab.com/kateops/kapigen/dsl/enum"
	"gitlab.com/kateops/kapigen/dsl/gitlab/job"
	"gitlab.com/kateops/kapigen/dsl/gitlab/pipeline"
	"gitlab.com/kateops/kapigen/dsl/gitlab/stages"
	"gitlab.com/kateops/kapigen/dsl/logger"
	"gitlab.com/kateops/kapigen/dsl/types"
)

func main() {
	pipelines.CreatePipeline(func(jobs *types.Jobs, mainPipeline *pipeline.CiPipeline) {
		// pipeline config
		mainPipeline.DefaultCiPipeline()
		//os.Setenv("LOGGER_LEVEL", "DEBUG")
		logger.DebugAny(mainPipeline)

		// test job config
		testJob := types.NewJob("test", "alpine", func(ciJob *job.CiJob) {
			logger.Info("test")
			// jobs config
			ciJob.AddScript("echo 'hello world'").
				SetStage(stages.TEST).
				Rules.AddRules(*job.DefaultPipelineRules([]string{"."}))
			ciJob.Tags.Add(enum.TagDefaultRunner)

		})

		// need job config
		needJob := types.NewJob("need", "alpine", func(ciJob *job.CiJob) {
			ciJob.AddScript("echo 'waiting for test'").
				SetStage(stages.TEST).
				Rules.AddRules(*job.DefaultPipelineRules([]string{"."}))
			ciJob.Tags.Add(enum.TagDefaultRunner)
		})

		// set dependency between jobs
		testJob.AddJobAsNeed(needJob)
		jobs.
			AddJob(testJob).
			AddJob(needJob)
	})
}

```
