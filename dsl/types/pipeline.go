package types

import (
	"os"

	"gitlab.com/kateops/kapigen/dsl/gitlab/job"
	"gitlab.com/kateops/kapigen/dsl/logger"
)

type PipelineType string

func (p PipelineType) String() string {
	return string(p)
}

type ConfigInterface interface {
	Validate() error
}
type PipelineConfigInterface interface {
	New() PipelineConfigInterface
	ConfigInterface
	Build(factory Factory, pipelineType PipelineType, Id string) (*Jobs, error)
	Rules() *job.Rules
}
type PipelineConfigTypeMap map[PipelineType]PipelineConfigInterface
type PipelineTypes struct {
	types PipelineConfigTypeMap
}

func NewPipelineTypes() *PipelineTypes {
	return &PipelineTypes{
		types: PipelineConfigTypeMap{},
	}
}

func (p *PipelineTypes) Add(pipelineType PipelineType, config PipelineConfigInterface) *PipelineTypes {
	if p.types[pipelineType] != nil {
		logger.Errorf("pipeline type %s already defined", pipelineType)
		os.Exit(1)
	}
	p.types[pipelineType] = config

	return p
}

func (p *PipelineTypes) Remove(pipelineType PipelineType) *PipelineTypes {

	if p.types[pipelineType] == nil {
		logger.Infof("pipeline type %s not defined, ignored", pipelineType)
	}
	delete(p.types, pipelineType)

	return p
}

func (p *PipelineTypes) GetTypesMap() PipelineConfigTypeMap {
	return p.types
}
