package types

type PipelineTypeConfigInterface interface {
	Decode(configTypes *PipelineConfigTypeMap) error
	GetType() PipelineType
}
