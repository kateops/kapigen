package pipelines_test

import (
	"testing"

	"github.com/gkampitakis/go-snaps/snaps"
	"gitlab.com/kateops/kapigen/dsl/factory"
	"gitlab.com/kateops/kapigen/dsl/gitlab/job"
	"gitlab.com/kateops/kapigen/dsl/gitlab/stages"
	"gitlab.com/kateops/kapigen/dsl/pipelines"
	"gitlab.com/kateops/kapigen/dsl/types"
	"gitlab.com/kateops/kapigen/dsl/version"
)

type TestPipelineConfig struct {
	ConfigId string `yaml:"id"`
}

func (t *TestPipelineConfig) New() types.PipelineConfigInterface {
	return &TestPipelineConfig{}
}
func (t *TestPipelineConfig) Validate() error {
	return nil
}

func (t *TestPipelineConfig) Build(_ types.Factory, _ types.PipelineType, _ string) (*types.Jobs, error) {
	return &types.Jobs{
		types.NewJob("test", "alpine", func(ciJob *job.CiJob) {
			ciJob.AddTagMediumPressure().
				AddScript("echo 'hello world'").
				SetStage(stages.TEST)
		}),
	}, nil
}

func (t *TestPipelineConfig) Rules() *job.Rules {
	return job.DefaultMainBranchRules()
}

func TestExtendPipelines(t *testing.T) {
	t.Run("extend pipeline functions", func(t *testing.T) {
		const TestPipeline types.PipelineType = "test"
		pipelines.PipelineConfigurationMap.Add(TestPipeline, &TestPipelineConfig{})

		settings := factory.NewSettings(
			factory.SetMode(version.GetModeFromString(version.Gitlab.Name())),
		)

		pipelineConfig := &types.PipelineConfig{
			Pipelines: []types.PipelineTypeConfig{
				{
					Type: TestPipeline,
					Config: TestPipelineConfig{
						ConfigId: "configId",
					},
					PipelineId: "testId",
				},
			},
		}

		pipelineJobs, err := types.LoadJobsFromPipelineConfig(factory.New(settings), pipelineConfig, pipelines.PipelineConfigurationMap)
		if err != nil {
			t.Error(err)
		}
		if len(pipelineJobs.GetJobs()) == 0 {
			t.Error("no jobs found")
		}

		snaps.MatchSnapshot(t, pipelineJobs)

	})
}
