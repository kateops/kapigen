package pipeline

import (
	"reflect"
	"strings"
	"testing"

	config2 "gitlab.com/kateops/kapigen/cli/pipelines/config"
	"gitlab.com/kateops/kapigen/cli/pipelines/jobs/docker"
	"gitlab.com/kateops/kapigen/cli/pipelines/jobs/php"
	"gitlab.com/kateops/kapigen/dsl/environment"
	"gitlab.com/kateops/kapigen/dsl/factory"
	types2 "gitlab.com/kateops/kapigen/dsl/types"
	"gitlab.com/kateops/kapigen/dsl/version"
)

func TestLoadJobsFromPipelineConfig(t *testing.T) {
	type args struct {
		factory        *factory.MainFactory
		pipelineConfig *types2.PipelineConfig
		configTypes    *types2.PipelineTypes
	}

	environment.CI_COMMIT_BRANCH.Set("feature/test")
	environment.CI_DEFAULT_BRANCH.Set("main")
	environment.CI_MERGE_REQUEST_LABELS.Set("version::patch")
	environment.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME.Set("feature/test")
	environment.CI_SERVER_URL.Set("https://gitlab.com")
	environment.CI_PROJECT_DIR.Set("/app")
	environment.CI_COMMIT_TAG.Set("1.0.0")
	mainFactory := factory.New(factory.NewSettings(
		factory.SetMode(version.Gitlab),
	))
	configTypes := types2.NewPipelineTypes()
	configTypes.Add(config2.PHPPipeline, &config2.Php{}).
		Add(config2.DockerPipeline, &config2.Docker{})
	tests := []struct {
		name    string
		args    args
		want    *types2.Jobs
		wantErr bool
	}{
		{
			name: "Can create php pipeline",
			args: args{
				factory:     mainFactory,
				configTypes: configTypes,
				pipelineConfig: &types2.PipelineConfig{
					Pipelines: []types2.PipelineTypeConfig{
						{
							Type: types2.PipelineType("php"),
							Config: config2.Php{
								Composer: config2.PhpComposer{
									Path: ".",
								},
								ImageName: "testImage",
							},
							PipelineId: "php",
						},
					},
				},
			},
			want: &types2.Jobs{
				func() *types2.Job {
					job, _ := php.NewPhpUnit("testImage", ".", "", ".", "", "./vendor/bin/phpunit", map[string]int32{})
					return job
				}(),
			},
			wantErr: false,
		}, {
			name: "Can create docker pipeline",
			args: args{
				factory:     mainFactory,
				configTypes: configTypes,
				pipelineConfig: &types2.PipelineConfig{
					Pipelines: []types2.PipelineTypeConfig{
						{
							Type: types2.PipelineType("docker"),
							Config: config2.Docker{
								Path:      ".",
								ImageName: "testImage",
							},
							PipelineId: "golang",
						},
					},
				},
			},
			want: &types2.Jobs{
				func() *types2.Job {
					job, _ := docker.NewDaemonlessBuildkitBuild("testImage", ".", ".", "Dockerfile", []string{"${CI_REGISTRY_IMAGE}:1.0.0", "${CI_REGISTRY_IMAGE}:latest"}, []string{})
					return job
				}(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := types2.LoadJobsFromPipelineConfig(tt.args.factory, tt.args.pipelineConfig, tt.args.configTypes)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadJobsFromPipelineConfig() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got.GetJobs()) != len(tt.want.GetJobs()) {
				t.Errorf("LoadFromPipelineConfig() got %d jobs, wanted %d", len(got.GetJobs()), len(tt.want.GetJobs()))
			}

			for i, job := range got.GetJobs() {
				if strings.Contains(tt.want.GetJobs()[i].GetName(), job.GetName()) {
					t.Errorf("LoadFromPipelineConfig() got job name %s, wanted %s", job.GetName(), tt.want.GetJobs()[i].GetName())
				}

				if !reflect.DeepEqual(job.CiJob.Script, tt.want.GetJobs()[i].CiJob.Script) {
					t.Errorf("LoadFromPipelineConfig() got job script %v, wanted %v", job.CiJob.Script.GetRenderedValue(), tt.want.GetJobs()[i].CiJob.Script.GetRenderedValue())
				}
			}
		})
	}
}
