package docker

import (
	"fmt"

	"gitlab.com/kateops/kapigen/dsl/logger"
)

type Image int

var DependencyProxy = ""

const (
	Kapigen_Latest Image = iota
	Alpine_3_18
	Terraform_Base
	Buildkit
	Buildkit_Rootless
	Crane_Debug
	Golang_1_21
	Golang_Golangci_Lint
)

var values = map[Image]struct {
	Image string
	Proxy bool
}{
	Kapigen_Latest:       {"registry.gitlab.com/kateops/kapigen/cli:latest", false},
	Alpine_3_18:          {"alpine:3.18", true},
	Terraform_Base:       {"hub.kateops.com/base/terraform:latest", false},
	Buildkit:             {"moby/buildkit:master", true},
	Buildkit_Rootless:    {"moby/buildkit:v0.12.3-rootless", true},
	Crane_Debug:          {"gcr.io/go-containerregistry/crane:debug", false},
	Golang_1_21:          {"golang:1.21", true},
	Golang_Golangci_Lint: {"golangci/golangci-lint:v1.59.1", true},
}

func (c Image) String() string {
	if value, ok := values[c]; ok {
		if value.Proxy {
			return fmt.Sprintf("%s%s", DependencyProxy, value.Image)
		}

		return value.Image
	}
	logger.Errorf("Not found for id: '%d'", c)

	if values[Alpine_3_18].Proxy {
		return fmt.Sprintf("%s%s", DependencyProxy, values[Alpine_3_18].Image)
	}
	return values[Alpine_3_18].Image
}
