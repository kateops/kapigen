package config

import (
	"gitlab.com/kateops/kapigen/dsl/pipelines"
	"gitlab.com/kateops/kapigen/dsl/types"
)

const (
	DockerPipeline types.PipelineType = "docker"
	PHPPipeline    types.PipelineType = "php"
	GOLANG         types.PipelineType = "golang"
	GENERIC        types.PipelineType = "generic"
)

func AddPipelinesConfigTypes() {
	pipelines.PipelineConfigurationMap.Add(DockerPipeline, &Docker{}).
		Add(GOLANG, &Golang{}).
		Add(PHPPipeline, &Php{}).
		Add(GENERIC, &Generic{})
}
